\section{Préparation et durcissement du système d'exploitation}

Dans cette section, l'installation du système, la sécurisation de l'administration du serveur et la mise en oeuvre d'une politique de mot de passe forts seront détaillées.

\subsection{Installation du système d'exploitation}

\subsubsection{Aucune interface graphique}
Le système d'exploitation retenu est CentOS 7 en version minimale, qui ne dispose pas d'interface graphique.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{install-minimal.png}
    \caption{Installation CentOS 7 minimale}
\end{figure}

\subsubsection{Schéma de partitionnement}
Le schéma de partitionnement sera fait manuellement en respectant les recommandations de l'ANSSI, et en s'adaptant au rôle du serveur, c'est à dire servir des fichiers.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{LVM.png}
    \caption{Utilisation de LVM pour faciliter la gestion des partitions}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{partionnement-lvm.png}
    \caption{Partitionnement manuel}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{chiffrement-disques.png}
    \caption{Ajout clef de chiffrement des disques}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{fstab.png}
    \caption{Ajout des options dans /etc/fstab}
\end{figure}



\subsubsection{Création de l'administrateur}
Aucun mot de passe administrateur ne sera défini afin que le compte root soit désactivé.
L'utilisateur qui va être crée sera administrateur. Chaque utilisateur aura son propre compte.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{users.png}
    \caption{Création d'un utilisateur administrateur et désactivation du compte root}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{mpires.png}
    \caption{Ajout du mot de passe et du rôle pour l'utilisateur}
\end{figure}

\subsubsection{Mise à jour}
Pour bénéficier des derniers correctifs de sécurité, une mise à jour du serveur est opérée.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{MAJ.png}
    \caption{Mise à jour du serveur}
\end{figure}

\subsubsection{Suppression des paquets inutiles}
Les paquets considérés comme inutiles vis-à-vis du rôle du serveur seront désinstallés.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{remove-packet.png}
    \includegraphics[width=0.8\textwidth]{remove-packet2.png}
    \caption{Désinstallation des paquets inutiles}
\end{figure}

\subsubsection{Protection du chargeur de démarrage}
La protection du chargeur de démarrage par un mot de passe comme recommandé par l'ANSSI,
est adaptée à notre cas, car cela fait partie du principe de défense en profondeur de protéger le démarrage et l'édition des options de boot contre l'altération.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{grub-set-password.png}
  \caption{Mise en place du mot de passe GRUB}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{centos7-grub2bootmenu-password.png}
    \caption{Saisie du mot de passe GRUB}
\end{figure}

\subsection{Sécurisation de l'administration du serveur}

\subsubsection{Renforcement de la configuration du serveur SSH}
Le serveur SSH est par défaut activé sur CentOS 7, avec connexion mot de passe.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{ssh.png}
    \caption{Connexion de l'administrateur en SSH}
\end{figure}

Dans un premier temps, on génère une biclé avec l'algorithme Ed25519 de taille 256 bits. Ed25519 offre une sécurité accrue et des performances supérieures comparé à RSA et ECDSA.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{generate-ssh-key.png}
  \caption{Génération de la biclé Ed25519}
\end{figure}

On ajoute ensuite la clé publique sur le serveur.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{add-ssh-key.png}
  \caption{Ajout de la clé publique sur le serveur pour l'administrateur}
\end{figure}

Sous CentOS 7, une commande supplémentaire est nécessaire pour rendre possible l'utilisation des clés pour la connexion SSH. 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{perms-ssh-auth.png}
  \includegraphics[width=0.8\textwidth]{ssh-perms-dir.png}
  \caption{Commande de restriction d'accès au dossier .ssh}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{ssh-success-key-connection.png}
  \caption{Connexion réussie avec la biclé}
\end{figure}

On renforce maintenant la configuration du serveur SSH en changeant les options du fichier /etc/ssh/sshd\_config.
\bigbreak

La connexion ne se fait qu'avec une biclé.
\begin{lstlisting}
  PasswordAuthentication no
\end{lstlisting}

La version 2 du protocole SSH est forcée.
\begin{lstlisting}
  Protocol 2
\end{lstlisting}

Le nombre de tentatives de connexions est limité.
\begin{lstlisting}
  LoginGraceTime 30
  MaxAuthTries 3
\end{lstlisting}

La connexion au compte root est désactivée.
\begin{lstlisting}
  PermitRootLogin no
\end{lstlisting}

Seul le compte de l'administrateur est autorisé à se connecter en SSH depuis le réseau local.
\begin{lstlisting}
  AllowUsers mpires@192.168.0.0/16
\end{lstlisting}

L'interface d'écoute est forcée et le port est modifié.
\begin{lstlisting}
  ListenAddress 192.168.11.31
  Port 1023
\end{lstlisting}

La redirection des flux est désactivée.
\begin{lstlisting}
  AllowTcpForwarding no
\end{lstlisting}

La redirection X11 est désactivée.
\begin{lstlisting}
  X11Forwarding no
\end{lstlisting}


\subsubsection{Bonnes pratiques concernant la biclef}
Les bonnes pratiques que l'administrateur devra appliquer pour sécuriser sa biclef sont :
\begin{itemize}
  \item S'assurer de l'accès personnel à sa clé privée en réduisant les permissions POSIX
  \item Mettre une passphrase sur sa clé privée pour la rendre inutilisable en cas de divulgation
\end{itemize}

\subsubsection{Configuration du firewall}

Le pare-feu est configuré à l'aide de UFW.
\bigbreak

Le flux sortant est droppé.
\begin{lstlisting}
  sudo ufw default deny outgoing
\end{lstlisting}

Le flux entrant est droppé.
\begin{lstlisting}
  sudo ufw default deny incoming
\end{lstlisting}

Les connexions SSH venant du réseau local sont autorisées.
\begin{lstlisting}
  sudo ufw allow in proto tcp from 192.168.0.0/16 to 192.168.11.31 port 22
\end{lstlisting}

Les connexions samba venant du réseau local sont autorisées.
\begin{lstlisting}
  sudo ufw allow in proto any from 192.168.0.0/16 to 192.168.11.31 port 445
\end{lstlisting}

Les connexions HTTP(S), DNS et NTP sortantes sont autorisées.
\begin{lstlisting}
  sudo ufw allow out proto tcp from 192.168.11.31 to any port 80
  sudo ufw allow out proto tcp from 192.168.11.31 to any port 443
  sudo ufw allow out proto any from 192.168.11.31 to any port 53
  sudo ufw allow out proto any from 192.168.11.31 to any port 123
\end{lstlisting}

On obtient la politique des flux suivante :

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{firewall.png}
  \caption{Règles firewall}
\end{figure}

\subsection{Politique de sécurité pour les mots de passe}

Les modules d'authentification enfichables (PAM) fournissent un mécanisme d'authentification centralisé 
que l'application système peut utiliser pour relayer l'authentification à une infrastructure configurée 
de manière centralisée.
PAM est enfichable car il existe un module PAM pour différents types d'authentification 
(telles que Kerberos, SSSD, NIS ou le système de fichiers local).

Dans notre cas, pour notre politique de mot de passe, le module PAM s'appelle "pwquality.so". Il remplace le module "cracklib.so" qui était très populaire.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{cat-system-auth.png}
    \caption{Détéction des modules PAM}
\end{figure}
Le module étant déja présent, il suffit de modifier le fichier /etc/security/pwquality.conf comme ceci :
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{pwquality-conf.png}
    \caption{Contenu du fichier pwquality}
\end{figure}
Les entrées du fichier pwquality que l'on va modifier sont les suivantes :
\begin{itemize}
    \item minlen = 10 Lorsque nous avons besoin d'un mot de passe d'une longueur d'au moins 10 caractères
    \item dcredit = -1 Lorsque nous avons besoin d'au moins 1 numéro
    \item ucredit = -1 Lorsque nous avons besoin d'au moins 1 lettre majuscule
    \item lcredit = -1 Lorsque nous avons besoin d'au moins 1 lettre minuscule
    \item ocredit = -1 Lorsque nous avons besoin d'au moins 1 caractère non alphanumérique
\end{itemize}

A l'arrivé nous faisons des tests sur nos mots de passe avec pwscore. 
C'est un module qui va venir tester les mots de passe pour vérifier s'ils rentrent dans les critères.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{pwscore.png}
    \caption{Test de mot de passe avec pwscore}
\end{figure}