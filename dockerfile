FROM alpine

RUN apk add --no-cache make texlive-full

RUN mkdir /build
WORKDIR /build

CMD sleep 9999