filename=main

pdf:
	pdflatex ${filename}
	bibtex ${filename}||true
	makeglossaries ${filename}||true
	pdflatex ${filename}
	pdflatex ${filename}
	mkdir -p dist
	mv ${filename}.pdf dist/
	rm -f main.ps main.log main.aux main.out main.dvi main.bbl main.blg main.acn main.glo main.ist main.lof main.toc main.fls main.fdb_latexmk main.gls main.alg main.glg main.acr main.lot

