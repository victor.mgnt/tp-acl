#!/bin/bash

# Creation des groupes
sudo groupadd direction
sudo groupadd pilotage
sudo groupadd comptabilite
sudo groupadd informatique
sudo groupadd logistique


# Creation des utilisateurs

function create_user {
  fullName=$1
  user=$2
  groups=$3

  sudo useradd $user --groups $groups --shell /usr/bin/nologin --no-create-home
  sudo usermod --lock $user
  sudo chfn --full-name "$fullName" $user
  password=$(</dev/urandom tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' | head -c 16  ; echo;)
  echo "The password for user $user is : $password" 
  (echo "$password"; echo "$password") | sudo smbpasswd -a -s $user 
}

create_user "Edouard-Franck Wagner" "efwagner" "direction,pilotage"

create_user "Alexandra Met" "amet" "pilotage,comptabilite"
create_user "Alfred Laroche" "alaroche" "comptabilite"
create_user "Tristan Maurice" "tmaurice" "comptabilite"

create_user "Claude Muller" "cmuller" "pilotage,informatique"
create_user "Henriette Julien" "hjulien" "informatique"
create_user "Jeanne Pasquier" "jpasquier" "informatique"
sudo usermod -aG informatique mpires
sudo chfn --full-name "Monique Pires" mpires

create_user "Gabriel Benard" "gbenard" "pilotage,logistique"
create_user "Cerise Lapresse" "clapresse" "logistique"
create_user "Richard Pirouet" "rpirouet" "logistique"
create_user "Jacques Bonnet" "jbonnet" "logistique"
create_user "Louis Lamoureux" "llamoureux" "logistique"
create_user "Honore Adler" "hadler" "logistique"