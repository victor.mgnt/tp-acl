#!/bin/bash
sudo ufw reset

sudo ufw default deny outgoing
sudo ufw default deny incoming
sudo ufw default deny routed

sudo ufw allow in proto tcp from 192.168.0.0/16 to 192.168.11.31 port 22
sudo ufw allow in proto any from 192.168.0.0/16 to 192.168.11.31 port 445

sudo ufw allow out proto tcp from 192.168.11.31 to any port 80
sudo ufw allow out proto tcp from 192.168.11.31 to any port 443
sudo ufw allow out proto any from 192.168.11.31 to any port 53
sudo ufw allow out proto any from 192.168.11.31 to any port 123

sudo ufw enable
sudo systemctl enable ufw