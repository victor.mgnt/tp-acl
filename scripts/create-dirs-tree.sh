#!/bin/bash

sudo mkdir -p /srv/{direction,comptabilite,administratif,dsi,depot,commun}
sudo mkdir -p /srv/direction/{strategies,achats}
sudo mkdir -p /srv/comptabilite/{comptabilite,tva,facturation}
sudo mkdir -p /srv/administratif/{gestion-conges,gestion-formations-internes,qualite,achats}
sudo mkdir -p /srv/dsi/{procedures,scripts,documentation}
sudo mkdir -p /srv/commun/{direction,administratif,informatique,production}